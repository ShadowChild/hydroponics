package shadowchild.mods.hydroponics.blocks.tile;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;


/**
 * @Author ShadowChild.
 */
public class TileEntityHydroPot extends TileEntity {

    private ItemStack storedFlower;

    @Override
    public void readFromNBT(NBTTagCompound nbt) {

        super.readFromNBT(nbt);

        NBTTagList tagList = nbt.getTagList("Flowers");

        for(int i = 0; i < tagList.tagCount(); ++i) {

            NBTTagCompound tagCompound = (NBTTagCompound)tagList.tagAt(i);
            this.storedFlower = ItemStack.loadItemStackFromNBT(tagCompound);
        }
    }

    @Override
    public void writeToNBT(NBTTagCompound nbt) {

        super.writeToNBT(nbt);

        NBTTagList tagList = new NBTTagList();

        if(this.storedFlower != null) {

            NBTTagCompound tagCompound = new NBTTagCompound();
            storedFlower.writeToNBT(tagCompound);
            tagList.appendTag(tagCompound);
        }
        nbt.setTag("Flowers", tagList);
    }

    public ItemStack getStoredFlower() {

        return storedFlower;
    }

    public void setStoredFlower(ItemStack stack) {

        this.storedFlower = stack;
    }
}
