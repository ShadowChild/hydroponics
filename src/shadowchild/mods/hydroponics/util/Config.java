package shadowchild.mods.hydroponics.util;

import net.minecraftforge.common.Configuration;

import java.io.File;


/**
 * @Author ShadowChild.
 */
public class Config {

    public static int hydroPot;

    public static void init(File configDir) {

        Configuration blocksCfg = new Configuration(new File(configDir.getParent(), "Hydroponics/blocks.cfg"));
        Configuration itemsCfg = new Configuration(new File(configDir.getParent(), "Hydroponics/items.cfg"));
        Configuration generalCfg = new Configuration(new File(configDir.getParent(), "Hydroponics/general.cfg"));

        try {

            blocksCfg.load();
            itemsCfg.load();
            generalCfg.load();

            hydroPot = blocksCfg.get("settings", "machine.hydroPot.id", 1000).getInt();

        } catch(Exception e) {

            e.printStackTrace();
        } finally {

            if(blocksCfg.hasChanged()) {

                blocksCfg.save();
            }

            if(itemsCfg.hasChanged()) {

                itemsCfg.save();
            }

            if(generalCfg.hasChanged()) {

                generalCfg.save();
            }
        }
    }
}
