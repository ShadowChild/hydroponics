package shadowchild.mods.hydroponics.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import shadowchild.mods.hydroponics.blocks.tile.TileEntityHydroPot;
import shadowchild.mods.hydroponics.util.Config;


/**
 * @Author ShadowChild.
 */
public class HPBlocks {

    public static Block hydroPot;

    public static void init() {

        hydroPot = new BlockHydroPot(Config.hydroPot);

        registerBlocks();
    }

    private static void registerBlocks() {

        GameRegistry.registerBlock(hydroPot, "hydroPot");
        GameRegistry.registerTileEntity(TileEntityHydroPot.class, "hydroPot");
    }
}
