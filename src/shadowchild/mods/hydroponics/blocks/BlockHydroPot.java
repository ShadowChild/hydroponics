package shadowchild.mods.hydroponics.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.BlockFlower;
import net.minecraft.block.material.Material;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import shadowchild.mods.hydroponics.api.implement.IPlant;
import shadowchild.mods.hydroponics.blocks.tile.TileEntityHydroPot;
import shadowchild.mods.hydroponics.util.HPUtils;
import shadowchild.mods.hydroponics.util.Strings;


/**
 * @Author ShadowChild.
 */
public class BlockHydroPot extends BlockContainer {

    public BlockHydroPot(int id) {

        super(id, Material.rock);
        this.setCreativeTab(HPUtils.tabHP);
        this.setUnlocalizedName(Strings.HYDROPONICS_POT);
    }

    @Override
    public TileEntity createNewTileEntity(World world) {

        return new TileEntityHydroPot();
    }

    @Override
    public String getUnlocalizedName() {

        return Strings.HYDROPONICS_POT;
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {

        TileEntity tile = world.getBlockTileEntity(x, y, z);

        if(world.isRemote) {

            return true;
        }

        if(tile != null && tile instanceof TileEntityHydroPot) {

            TileEntityHydroPot pot = (TileEntityHydroPot)tile;

            ItemStack stack = player.getCurrentEquippedItem();
            if(stack != null) {

                // Stupid Check Is Stupid
                if(pot.getStoredFlower() == null) {

                    Block plant = Block.blocksList[stack.itemID];

                    if(plant != null && (plant instanceof BlockFlower || plant instanceof IPlant)) {

                        ItemStack toStore = new ItemStack(stack.itemID, 1, stack.getItemDamage());
                        if(!player.capabilities.isCreativeMode) {

                            --stack.stackSize;
                        }
                        pot.setStoredFlower(toStore);
                    }

                    System.out.println(pot.getStoredFlower().itemID + " " + pot.getStoredFlower().getItemDamage());
                }
            } else {

                if(pot.getStoredFlower() != null) {

                    System.out.println("stored id: " + pot.getStoredFlower().itemID + " meta of: " + pot.getStoredFlower().getItemDamage());
                }
            }
        }

        return true;
    }

    @Override
    public void breakBlock(World world, int x, int y, int z, int par5, int par6) {

        TileEntity tile = world.getBlockTileEntity(x, y, z);

        if(tile != null && tile instanceof TileEntityHydroPot) {

            TileEntityHydroPot pot = (TileEntityHydroPot)tile;

            if(pot.getStoredFlower() != null) {

                EntityItem toDrop = new EntityItem(world, x, y, z, pot.getStoredFlower());
                if(!world.isRemote) {

                    world.spawnEntityInWorld(toDrop);
                    System.out.println("spawning item with id: " + pot.getStoredFlower().itemID + " and meta of: " + pot.getStoredFlower().getItemDamage());
                }
            }
        }
        world.removeBlockTileEntity(x, y, z);
    }
}