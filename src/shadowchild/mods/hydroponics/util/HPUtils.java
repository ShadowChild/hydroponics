package shadowchild.mods.hydroponics.util;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import shadowchild.mods.hydroponics.blocks.HPBlocks;


/**
 * @Author ShadowChild.
 */
public class HPUtils {

    public static final String MOD_ID = "hydroponics";
    public static final String MOD_NAME = "Hydroponics";
    public static final String MOD_VERSION = "%VERSION%";

    public static CreativeTabs tabHP = new CreativeTabHP();

    public static class CreativeTabHP extends CreativeTabs {

        public CreativeTabHP() {

            super("HP");
        }

        @Override
        public String getTranslatedTabLabel() {

            return Strings.CREATIVE_TAB;
        }

        @Override
        public ItemStack getIconItemStack() {

            return new ItemStack(HPBlocks.hydroPot);
        }
    }
}
