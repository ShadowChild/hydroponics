package shadowchild.mods.hydroponics.util;

/**
 * @Author ShadowChild.
 */
public class Strings {

    // Blocks
    public static final String HYDROPONICS_POT = "machine.hydroPot";

    // Misc
    public static final String CREATIVE_TAB = "tab.hydroponics.name";
}
